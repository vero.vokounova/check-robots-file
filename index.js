import express from "express";
import cors from "cors"; // povoleni Cross-Origin Resource Sharing
import axios from "axios";
import fs from "fs";

const domains = [
  "diamondsalon.cz",
  "jan-kocka.cz",
  "jakpsatweb.cz",
  "cofis.cz",
];
const app = express();
const port = 5500;

app.use(cors());

//Funkce, která hlídá změny v souboru robots.txt
const checkChanges = (domain) => {
  const url = `https://${domain}`;
  axios(`${url}/robots.txt`)
    .then((response) => {
      if (response.status === 200) {
        const infoMessage = `${url} obsahuje soubor robots.txt`;
        console.log(infoMessage);
        saveTxtAvailability(
          infoMessage,
          `${domain}_robotsAvailability.txt` //uloží zprávu o dostupnosti do robotsAvailability
        );
        const currentContent = response.data; // data se uloží do proměnné currentContent
        //console.log(currentContent);

        fs.readFile(
          `data/contentTxt/${domain}_previousContent.txt`,
          "utf8",
          (err, previousContent) => {
            //callback - spustí se po dokončení čtení souboru, pokud čtení proběhlo úspěšně - uloží se do previousContent
            if (err) {
              console.error(
                `Chyba při čtení souboru ${domain}_previousContent.txt: ${err}`
              );
              return;
            }
            if (previousContent !== currentContent) {
              console.log(
                `Změna v souboru ${domain}_previousContent.txt byla zjištěna.`
              );
              saveToFile(currentContent, `${domain}_previousContent.txt`);
              findChanges(previousContent, currentContent);
            }
          }
        );
      }
    })
    .catch((err) => {
      const infoMessage = `${url} neobsahuje soubor robots.txt`;
      console.log(infoMessage);
      saveTxtAvailability(infoMessage, `${domain}_robotsAvailability.txt`);
    });
};

//Ukládá info o dostupnosti souboru txt
const saveTxtAvailability = (content, fileName) => {
  fs.writeFile(`data/availability/${fileName}`, content, (err) => {
    if (err) {
      console.error(`Chyba při zápisu souboru ${fileName}: ${err}`);
      return;
    }
    console.log(`Informace o ${fileName} byla uložena`);
  });
};

//Ukládá obsah txt souborů do složky contentTxt
const saveToFile = (content, fileName) => {
  fs.writeFile(`data/contentTxt/${fileName}`, content, (err) => {
    if (err) {
      console.error(`Chyba při zápisu souboru ${fileName}: ${err}`);
      return;
    }
    console.log(`Obsah souboru ${fileName} byl úspěšně uložen.`);
  });
};

//Porovnání předchozího a aktálního obsahu robots.txt
const findChanges = (previousContent, currentContent) => {
  const previousLines = previousContent.split("\n");
  const currentLines = currentContent.split("\n");

  console.log("Nalezeny změny v souboru:");
  for (let i = 0; i < previousLines.length; i++) {
    if (previousLines[i] !== currentLines[i]) {
      console.log(`Změna na řádku ${i + 1}:`);
      console.log(`Předchozí obsah: ${previousLines[i]}`);
      console.log(`Nový obsah: ${currentLines[i]}`);
    }
  }
};

setInterval(() => {
  domains.forEach((domain) => {
    checkChanges(domain);
  });
}, 60000);

app.listen(port, () => {
  console.log(`Server běží na http://localhost:${port}`);
});
